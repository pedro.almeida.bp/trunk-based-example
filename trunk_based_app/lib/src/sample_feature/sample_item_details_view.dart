import 'package:flutter/material.dart';
import 'package:trunk_based_app/src/core/feature_flag_manager.dart';

/// Displays detailed information about a SampleItem.
class SampleItemDetailsView extends StatefulWidget {
  const SampleItemDetailsView({Key? key}) : super(key: key);

  static const routeName = '/sample_item';

  @override
  State<SampleItemDetailsView> createState() => _SampleItemDetailsViewState();
}

class _SampleItemDetailsViewState extends State<SampleItemDetailsView> {
  @override
  Widget build(BuildContext context) {
    final featureFlag = FeatureFlagManager();
    featureFlag.fetch();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Item Details'),
      ),
      body: Column(
        children: [
          Center(
            child: Text(
                'Example param 1: ${featureFlag.getInt('example_param_1').toString()}'),
          ),
          MaterialButton(
              child: const Text('Fetch now'),
              onPressed: () async {
                await featureFlag.fetchNow();
                setState(() {});
              })
        ],
      ),
    );
  }
}
