import 'package:flutter/material.dart';
import 'package:trunk_based_app/src/sample_feature/sample_item_list_view.dart';
import 'package:trunk_based_app/src/splash/splash_controller.dart';

class SplashView extends StatefulWidget {
  static const routeName = '/splash';

  const SplashView({Key? key}) : super(key: key);

  @override
  State<SplashView> createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  final _controller = SplashController();

  @override
  void initState() {
    super.initState();
    _initializeDependenciesAndRedirect();
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Text('Loading app...'),
      ),
    );
  }

  void _initializeDependenciesAndRedirect() async {
    await _controller.startDependencies();
    _goToMainPage();
  }

  void _goToMainPage() {
    Navigator.pushReplacementNamed(context, SampleItemListView.routeName);
  }
}
