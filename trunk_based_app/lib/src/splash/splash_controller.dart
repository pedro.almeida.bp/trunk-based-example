import 'package:flutter/widgets.dart';
import 'package:trunk_based_app/src/core/feature_flag_manager.dart';
import 'package:trunk_based_app/src/sample_feature/sample_item_list_view.dart';

class SplashController {
  Future<void> startDependencies() async {
    await FeatureFlagManager().initialize();
  }
}
