import 'package:firebase_remote_config/firebase_remote_config.dart';

abstract class FeatureFlagManager {
  factory FeatureFlagManager() => FeatureFlagManagerImpl();

  final Map<String, dynamic> defaultValues = {};
  Future<void> initialize();
  Future<void> fetch();
  Future<void> fetchNow();
  int getInt(String key);
}

class FeatureFlagManagerImpl implements FeatureFlagManager {
  final remoteConfig = FirebaseRemoteConfig.instance;

  @override
  Map<String, dynamic> get defaultValues => {
        "example_param_1": 42,
        "example_param_2": 3.14159,
        "example_param_3": true,
        "example_param_4": "Hello, world!",
      };

  @override
  Future<void> initialize() async {
    await remoteConfig.setConfigSettings(RemoteConfigSettings(
      fetchTimeout: const Duration(minutes: 1),
      minimumFetchInterval: const Duration(minutes: 5),
    ));
    await remoteConfig.setDefaults(defaultValues);
    await remoteConfig.fetchAndActivate();
  }

  @override
  int getInt(String key) {
    return remoteConfig.getInt(key);
  }

  @override
  Future<void> fetch() async {
    await remoteConfig.fetchAndActivate();
  }

  @override
  Future<void> fetchNow() async {
    try {
      await _invalidateCache();
      await remoteConfig.fetchAndActivate();
      await _setDefaultCacheValidity();
    } catch (_) {
      print('falha ao dar fetch');
    }
  }

  Future<void> _invalidateCache() async {
    await _updateCacheValid(duration: const Duration(seconds: 0));
  }

  Future<void> _setDefaultCacheValidity() async {
    await _updateCacheValid(duration: const Duration(minutes: 5));
  }

  Future<void> _updateCacheValid({required Duration duration}) async {
    await remoteConfig.setConfigSettings(RemoteConfigSettings(
      fetchTimeout: const Duration(minutes: 1),
      minimumFetchInterval: duration,
    ));
  }
}
